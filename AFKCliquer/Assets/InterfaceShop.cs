﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InterfaceShop : MonoBehaviour
{
    public List<GameObject> inputShopList = new List<GameObject>();
    private GameObject selectedShop;


    public void SelectShop(GameObject btn_Shop)
    {
        if(selectedShop != null)
        {
            selectedShop.SetActive(false);
        }

        selectedShop = btn_Shop;
        btn_Shop.SetActive(true);
    }
}
